import psutil


class DiskMonitor:
    def __init__(self):
        pass

    def get_partiton_list(self):
        return [drive.device for drive in psutil.disk_partitions()]

    def get_partition_used(self, partition):
        return psutil.disk_usage(partition).used

    def get_patition_free(self, partition):
        return psutil.disk_usage(partition).free

